2015-03-04
	* CSV-Import bugfix in order to allow import networks with non-asccii characters
	* enhance (database) documentation and test cases

2015-02-16
	* add test suite for XMLRPC-API
	* add root methods (add, edit, delete) to XMLRPC-API

2015-02-15
	* add simple REST wrapper
	* add config option showNetBorders
	* add config option removeCIDRFromIPs
	* minor Bugfixes

2015-02-13
	* Add multiple authentication methods for login at once

2015-02-07
	* add some flushes to the session object
	* add timeout to 'showStatus'-Ajax-call, in order to circumvent a race condition (expanded net don't stay expanded)
	* when you define a minimal network cidr, you can expand that net even if it has no childs

2015-01-03
	* Add tag support for networks

2014-12-31
	* remove potential XSS attack vectors

2014-10-27
	* fix import status issue
	* limit height of warning window

2014-10-18
	* add export subnets for supernets and roots
	* add wrapper for accessing CGI params

2014-09-14
	* added simple 'exportSubnet' method
	added new config variable 'gui/showNrOfFreeSubnetsWithMinCIDR'

2014-09-09
	* Bugfix (calculation of free subnets)
	* add 'editNet' function to XMLRPC-API
	* add functionality to search for networks (with wildcards)
	
2014-09-04
	* add new XMLRPC-API method: editNet

2014-08-28
	* add DB-Docu
	* add mysql support for networkLock
	* some Bugfixes
	* add default network lock configuration item

2014-08-27
	* add locking for network deletion
	* add new API method: XMLRPC
	* improve rights delegation while editing networks
	* add new button 'Search and assign free networks'
	* add possibility to configure multiple server to ldap authentication
	* inherit ACS from super net / root
	* sort user and group field
	* improve robustness against concurrent requests

2012-09-26
	* added search for free network in GUI
	* several Bugfixes

2012-07-23
	* added audit functionality

2012-06-20
	* added possibility to show amount of free subnets in search view
	* added possibility to change a template name
	* added yum support for checkPerlDependencies
	* enhance ldap authentication support (authenticate a DN after searching for it in LDAP)
	* Expanded entries field in templateEntry to 1024 chars

2012-01-31
	* enhanced generic database support
	* added postgresql support
	* some speedup enhancements

2012-01-17
	* start enhancing database abstraction in order to implement other database accessors

2012-01-13
	* several Bugfixes

2011-07-11
	* added recurrent mode for plugin 'GenZonefile' in order to create zonefiles automatically

2011-07-09
	* added parameter for onDemand execution of plugins
	* added genZonefile plugin

2011-07-07
	* added ldap authentication module

2011-06-12
	* force string type in SOAP-API (Patch-ID:3314242)

2011-02-15
	* Bugfix 3168045, 3111556

2010-09-24
	* add support for 32 bit as numbers

2010-06-23
	* Bugfixes (editRoot, copyNets, moveNets)
	* Enhancements (showStatus, importASN, importCSV)

2010-06-22
	* cleanup database tool

2010-03-19
	* Bugfixes (checkNetworkState, css-id Dups)

2010-02-26
	* remove circular dependencies

2010-02-24
	* several Bugfixes (ula/bit mask issues)
	* test-page for dependency checking

2010-02-15
	* CSS - bugfixes

2010-02-13
	* Rewrite of database upgrade code (in order to make MySQL::Diff obsolete)

2010-02-11
	* Unicode handling (Support for multibyte languages)

2010-01-12
	* add italien localization (thanks to Pf)
	* improve csv-importer
	* bugfix - deleting networks/roots

2010-01-10
	* Validation of a consistent network state while creating a new network (i.e.  only one assignment per allocation)
	* update available network states

2009-11-30
	* Foundry Config importer
	* enhance Cisco Config importer

2009-10-31
	* Juniper Config importer

2009-05-5
	* Improved checking if DB has changed

2008-11-12
	* HaCiAPI - SOAP Interface for HaCi

2008-10-22
	* Search for Template Values

2008-08-07
	* Performance Tuning (Improved Cache Management)

2008-07-19
	* DNS Import improved (error messages, ptr and aaaa records)

2008-06-27
	* Configuration per User (-Interface)
	* Ability to change own Password
	* Filter Search with Network States

2008-06-25
	* Show Tree Structure
	* Fuzzy Search

2008-06-20
	* option to define default Subnet Size (in CIDR notation)

2008-06-14
	* show (free) Subnets (with fixed CIDR)

2008-04-23
	* New Configfile Format

2008-04-10
	* New Configfile Mechanism

2008-03-30
	* HaCid Infos

2008-03-18
	* Dependencies Checking Script

2008-03-07
	* floating Windows

2008-03-05
	* 6 Plugins available

2008-02-28
	* add dynamic Descriptions with Plugin Variables

2008-01-08
	* HaCi Daemon

2007-12-20
	* add Plugin Infrastructure

2007-10-31
	* advanced Status Messages (Ajax)

2007-10-20
	* Buttons with Images
	* Layout optimations

2007-10-15
	* add third column to treeview for direct access to network functions
	* rewrite of HaCiBox Templates

2007-10-08
	* add IPv6 Support
	* recurrent Plugin execution
	* one more color Scheme
	* automatic modification of Database Schemas

2007-09-03
	* add Combine functionality

2007-09-01
	* Bugfix (deleting networks)
	* Possibility to split networks

2007-08-31
	* add possibility to modify also netaddress and netmask of networks 

2007-08-29
	* expand states to Free, Reserved, Locked
	* Add State Colors/Images

2007-08-27
	* add AJAX Support

2007-03-29
	* add 'Flush Cache' Button
	* fix ACL Handling

2006-11-18
	* import CSV Configs with "net-Type" support
	* ACL Caching Machanism (No 'Flush ACL' needet any more)
	* Some Bugfixes & GUI update

2006-11-15
	* First Release
