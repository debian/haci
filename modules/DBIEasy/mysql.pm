package DBIEasy::mysql;

use strict;
use warnings;

use base 'DBIEasy';

my $queryTimeout	= 300;

sub getDSN {
	my $self	= shift;

	return (
		'DBI:mysql:' .
		'database=' . $self->dbname .
		';host=' . $self->dbhost,
		$self->dbuser, $self->dbpass
	);
}

sub checkIfTableExists {
	my $self	= shift;
	my $dbh		= $self->dbh;
	my $table	= $self->TABLE;

	my @tables	= $dbh->tables(undef, undef, $table);
	unless (grep {/^(`?$self->{dbname}`?.)?`?$table`?$/} @tables) {
		if ($self->SETUPTABLE) {
			$self->_carp("Table '$table' doesn't exist, try to create it...\n");
			my $createStmt	= "CREATE TABLE `$table` (" . $self->CREATETABLE . ') ENGINE=InnoDB DEFAULT CHARSET=utf8';
			$dbh->do($createStmt)	or $self->_croak($dbh->errstr);
			
			my @tables	= $dbh->tables(undef, undef, $table);
			unless (grep {/^(`?$self->{dbname}`?.)?`?$table`?$/} @tables) {
				$self->_croak("Sorry, after creating the Table '$table', it isn't there?");
				return 0;
			}
		} else {
			$self->_croak("Table '$table' doesn't exist!");
			return 0;
		}
	}
	return 1;
}

sub getPK {
	my $self	= shift;
	my $dbh		= $self->dbh;
	my $table	= $self->TABLE;
	my @PKs		= ();

	my $sth = $dbh->prepare("SHOW INDEX FROM $table");
  $sth->execute;
	$self->_croak($dbh->errstr) if $dbh->errstr;
  if ($sth) {
		my $hashRef = $sth->fetchall_hashref(['Key_name', 'Seq_in_index']);
	  foreach (keys %{$hashRef}) {
	  	my $key	= $_;
			foreach (keys %{$hashRef->{$key}}) {
		  	if ($key eq 'PRIMARY') {
		  		push @PKs, $hashRef->{$key}->{$_}->{Column_name};
		  	}
			}
	  }
  }
	if ($#PKs == -1) {
		$self->_croak("No Primary Keys in Table '$table'!");
	} else {
		return \@PKs;
	}
}

sub getShowCreate {
	my $self	= shift;
	my $dbh		= $self->dbh;
	my $table	= $self->TABLE;
	
	my $sth	= $dbh->prepare("SHOW CREATE TABLE `$table`");
	$sth->execute;
	$self->_croak($dbh->errstr) if $dbh->errstr;

	if ($sth) {
		my $showCreatet	= $sth->fetchall_arrayref([1]);
		if (defined $showCreatet) {
			my $showCreate	= $$showCreatet[0];
			return $$showCreate[0];
		} else {
			$self->_carp("Sorry, Show Create Table '$table' doesn't work!");
			return '';
		}
	}
}


sub getColoumns {
	my $self	= shift;
	my $dbh		= $self->dbh;
	my $table	= $self->TABLE;
	
	my $sth	= $dbh->prepare("DESCRIBE `$table`");
	$sth->execute;
	$self->_croak($dbh->errstr) if $dbh->errstr;

	if ($sth) {
		my $hashRef	= $sth->fetchall_hashref('Field');
		return ((defined $hashRef && ref $hashRef eq 'HASH') ? keys %{$hashRef} : ());
	} else {
		$self->_carp("Sorry, no Columns found!");
		return ();
	}
}

sub checkForOldHandles {
	my $self	= shift;
	my $dbh		= shift;

	my $sth_sh_proc	= $dbh->prepare("show full processlist");
	my $sth_kill		= $dbh->prepare("kill ?");

	unless ($sth_sh_proc->execute()) {
		_carp("Unable to execute show procs [" . $dbh->errstr() . "]");
		return 0;
	}
	while (my $row = $sth_sh_proc->fetchrow_hashref()) {
		if ($row->{Command}	eq 'Sleep' && $row->{Time} > $queryTimeout) {
			my $id	= $row->{Id};
			$sth_kill->execute($id);
		}
	}

	return 1;
}

sub mkCaseSensitive {
	my $self			= shift;
	my @whereStr	= @_;

	return map {$_ = 'binary ' . $_} @whereStr;
}

sub _log {
	DBIEasy::_log(@_);
}

sub _carp {
	DBIEasy::_carp(@_);
}

sub _croak {
	DBIEasy::_croak(@_);
}

1;

# vim:ts=2:sts=2:sw=2
