package HaCi::Tables::postgresql::networkV6;
use base 'DBIEasy::postgresql';

sub TABLE { #Table Name
	'network_v6'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID						=> 'id',
		rootID				=> 'root_id',
		networkPrefix	=> 'network_prefix',
		hostPart			=> 'host_part',
		cidr					=> 'cidr'
	}
}

1;

# vim:ts=2:sw=2:sws=2
