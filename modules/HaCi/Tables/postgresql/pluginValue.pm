package HaCi::Tables::postgresql::pluginValue;
use base 'DBIEasy::postgresql';

sub TABLE { #Table Name
	'plugin_value'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID						=> 'id',
		netID					=> 'net_id',
		pluginID			=> 'plugin_id',
		origin				=> 'origin',
		name					=> 'name',
		value					=> 'value',
	}
}

1;

# vim:ts=2:sw=2:sws=2
