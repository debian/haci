package HaCi::Tables::postgresql::network;
use base 'DBIEasy::postgresql';

sub TABLE { #Table Name
	'network'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	0
}

sub CREATETABLE { # Table Create Definition
}

sub columnMapping { # map DBIEasy methods to table column names
	{
		ID						=> 'id',
		rootID				=> 'root_id',
		network				=> 'network',
		description		=> 'description',
		state					=> 'state',
		defSubnetSize	=> 'def_subnet_size',
		tmplID				=> 'tmpl_id',
		ipv6ID				=> 'ipv6_id',
		searchStr			=> 'search_str',
		createFrom		=> 'create_from',
		createDate		=> 'create_date',
		modifyFrom		=> 'modify_from',
		modifyDate		=> 'modify_date',
	}
}

1;

# vim:ts=2:sw=2:sws=2
