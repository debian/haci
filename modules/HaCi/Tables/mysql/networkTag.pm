package HaCi::Tables::mysql::networkTag;
use base 'DBIEasy::mysql';

sub TABLE { #Table Name
	'networkTag'
}

sub SETUPTABLE { # Create Table unless it doesn't exists?
	1
}

sub CREATETABLE { # Table Create Definition
	q{
  `ID` integer NOT NULL auto_increment,
	`netID` integer NOT NULL default '0',
	`tag` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`ID`),
	UNIQUE KEY (`netID`, `tag`)
	}
}

sub _log {
	my $self	= shift;
	my @msg		= @_;
	
	DBIEasy::mysql::_log($self, @msg);
}

sub _carp {
	my $self							= shift;
	my ($message, %info)	= @_;

	DBIEasy::mysql::_carp($self, $message, %info);
}

sub _croak {
	my $self							= shift;
	my ($message, %info)	= @_;

	DBIEasy::mysql::_croak($self, $message, %info);
}

1;
