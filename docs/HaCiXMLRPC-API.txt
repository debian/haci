Class XMLRPC
    This class provides a XML-RPC API for HaCi.

    Basic usage:

            my $api         = new Frontier::Client(url => "http://$server/RPC2");
            my $session     = $api->call('login', [$user, $pass]);
            die 'Login failed!' unless $session;

            print Dumper($api->call($method, $session, @args));

            $api->call('logout', $session);

            exit 0;

    login()
        Login and return your session

         Params

         Returns
            - session        [string]    session token you can use for authentication

    logout()
        Logout

         Params

         Returns

    search()
        Search networks

         Params
            - search          [string]  string to search for
            - state           [string]  filter the result by that network state (ALLOCATED PA, ASSIGNED, ...)
            - exact           [string]  don't do a substring search
            - template name   [string]  filter the result by that template name
            - template query  [hash]    filter the result by defining special values for template entries (i.e.: {name => 'POOL1'})
            - root name       [string]  limit the search in that root
            - nrOfFreeSubs    [boolean] compute the number of free subnets in each found network
            - withDetails     [boolean] show details for found networks
            - tags            [string]  return only networks with this tags, seperate with spaces (i.e.: 'OR foo bar', 'AND foo bar')
         Returns
            - networks        [array]    array of found network blocks (hash)
                                         - netID
                                         - network
                                         - rootName
                                         - description
                                         - state
                                         - nrOfFreeSubs
         Example
            my $networks = search('Test', 'ASSIGNED', 0, 'DSL-POOL', {name => 'Pool1'}, 'DSL-Test-Pool-Root', 0);

    getFreeSubnets()
        Find free subnets in a specified root and supernet and return the
        wanted amount of subnets

         Params
            - root name       [string]  search free subnets in that root
            - supernet        [network] search free subnets in that supernet (e.g. 192.168.0.0/24, 2001::/120)
            - cidr            [integer] specify the target cidr of your subnets (e.g. 30)
            - amount          [integer] how many subnets you want to get
         Returns
            - networks        [array]   array of free subnets found (hash)
                                        - network
         Example
            my $freeSubnets = getFreeSubnets('Test root', '10.184.120.0/23', 32, 1);

    getFreeSubnetsFromSearch()
        Search networks and return free subnets from them

         Params
            - search          [string]  string to search for
            - state           [string]  filter the result by that network state (ALLOCATED PA, ASSIGNED, ...)
            - exact           [string]  don't do a substring search
            - template name   [string]  filter the result by that template name
            - template query  [hash]    filter the result by defining special values for template entries (i.e.: {name => 'POOL1'})
            - root name       [string]  limit the search in that root
            - cidr            [integer] specify the target cidr of your subnets (e.g. 30)
            - amount          [integer] how many subnets you want to get
         Returns
            - networks        [array]   array of free subnets found (hash)
                                        - network
                                        - root name
         Example
            my $freeSubnets = getFreeSubnetsFromSearch('search me', '', 1, '', {}, 'Test-Root', 29, 3);

    listRoots()
        Add a root to HaCi

         Params
         Returns
            - roots        [array]    array of found roots (hash)
                                         - ID
                                         - name
                                         - ipv6 (boolean)

    addRoot()
        Add a root to HaCi

         Params
            - root name       [string]  add a root with this name
            - description     [string]  description of the root
            - ipv6            [boolean] this root contains IPv6 networks
         Returns
            - success         [string] 0 on success, error-String at error

    editRoot()
        Edit an existing root

         Params
            - root name      [string]  edit this root
            - new root name  [string]  change root name
            - description    [string]  new description of the root
         Returns
            - success         [string] 0 on success, error-String at error

    delRoot()
        Delete a root from HaCi

         Params
            - root name       [string]  remove the root
         Returns
            - success         [string] 0 on success, error-String at error

    addNet()
        Add a network to HaCi

         Params
            - root name       [string]  add the network to this root
            - network         [network] add this network (e.g. 192.168.0.1/32, 2001::dead:beef:0/125)
            - description     [string]  description of the network
            - state           [string]  state of the network (e.g. ALLOCATED PA, ASSIGNED, ...)
            - def subnet Size [integer] define a default subnet cidr size (e.g. 30)
            - template name   [string]  assign a template to the network
            - template values [hash]    pass template values (e.g. {hostname => 'abc.de', os => 'redhat'})
            - tags                                              [string]  add tags (seperate by comma)
         Returns
            - success         [string] 0 on success, error-String at error

    editNet()
        Edit an existing network

         Params
            - root name   [string]  edit the network in this root
            - network     [network] edit this network (e.g. 192.168.0.1/32, 2001::dead:beef:0/125)
            - changes     [hash]    changes (valid attributes are: network description state rootName defSubnetSize tmplName tmplValues)
                                    e.g.:
                                     {description => 'test_8'}
                                     {network => '192.168.0.8/29', defSubnetSize=>30, state=>'FREE', rootName=>'larsux.de'}
         Returns
            - success         [string] 0 on success, error-String at error

    delNet()
        Delete a network from HaCi

         Params
            - root name       [string]  remove the netork from this root
            - network         [network] remove this network (e.g. 192.168.0.1/32, 2001::dead:beef:0/125)
            - network lock    [integer] lock network for X seconds
            - withSubnets     [boolean] also remve subnets
         Returns
            - success         [string] 0 on success, error-String at error

    assignFreeSubnet()
        Find the next free subnet and assign it in one step

         Params
            - root name       [string]  search free subnets in that root
            - supernet        [network] search free subnets in that supernet (e.g. 192.168.0.0/24, 2001::/120)
            - cidr            [integer] specify the target cidr of your subnets (e.g. 30)
            - description     [string]  description of the network
            - state           [string]  state of the network (e.g. ALLOCATED PA, ASSIGNED, ...)
            - def subnet Size [integer] define a default subnet cidr size (e.g. 30)
            - template name   [string]  assign a template to the network
            - template values [hash]    pass template values (e.g. {hostname => 'abc.de', os => 'redhat'})
         Returns
            - network details [hash]    new network assigned:
                                        - netID
                                        - network
                                        - modify date
                                        - ipv6 (boolean)
                                        - description
                                        - state
                                        - create from
                                        - create date
                                        - default subnet cidr size
                                        - template name
                                        - modify from

    getNetworkDetails()
        Get details from a network in HaCi

         Params
            - root name       [string]  get details of a network in this root
            - network         [network] get details of this network (e.g. 192.168.0.1/32, 2001::dead:beef:0/125)
         Returns
            - network         [hash]    network details:
                                        - netID
                                        - network
                                        - modify date
                                        - ipv6 (boolean)
                                        - description
                                        - state
                                        - create from
                                        - create date
                                        - default subnet cidr size
                                        - template name
                                        - modify from

    getSubnets()
        Get subnets from a root and optional from a supernet

         Params
            - root name       [string]  get subnets from this root
            - supernet        [network] get subnets from this network (e.g. 192.168.0.1/32, 2001::dead:beef:0/125)
         Returns
            - networks        [array]    array of found networks (hash)
                                         - netID
                                         - network
                                         - modify date
                                         - ipv6 (boolean)
                                         - description
                                         - state
                                         - create from
                                         - create date
                                         - default subnet cidr size
                                         - template name
                                         - modify from
         Example
            my $subnets = getSubnets('Test');
            my $subnets = getSubnets('Test', '192.168.0.0/24');

